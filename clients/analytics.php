<?php 
ini_set('display_errors',1);
require_once("../global/session_start.php");
ft_check_permission("client");

   if ((!isset($_SESSION["ft"]["account"]["submission_id"]) || empty($_SESSION["ft"]["account"]["submission_id"])) &&
       empty($_SESSION["ft"]["account"]["account_id"]))
     {
        header("location: $g_root_url/modules/submission_accounts/logout.php");
      $message_flag = "notify_invalid_account_information_in_sessions";
      ft_logout_user($message_flag);
     }

$_SESSION['url'] = $g_root_url;

_ft_cache_form_stats();

//$results2 = ft_get_submission_results2(4, "col_1");
$results2 = ft_get_submission_results3(6, 2);

//keep this :)
$forms = ft_get_all_forms();
//for each form
$student_numbers = array("", "");
foreach ($forms as $form)
{
	$questions = ft_get_questions($form['form_id']);
	foreach ($questions as $question) 
	{
		$column = preg_replace('/col_/', '', $question['col_name']);
		$answers = ft_get_submission_results3($form['form_id'], $column);
		foreach ($answers as $answer => $result)
		{
			$_SESSION["results"][$form['form_name']][$question['field_title']][$answer] = $result;
			//echo "form name: " . $form['form_name'] . " question: " . $question['field_title'] . " answer " . $answer . " result: " . $result . "<br>";
		}
	}
}
$numbers = ft_get_student_numbers(6);
foreach ($numbers as $no)
{
	echo $no;
}
/*if ($_SESSION['form1'] == NULL) {
	$_POST['form1'] = 0;
	$_SESSION['form1'] = 0;
}
if ($_SESSION['questions'] == NULL) {
	$_SESSION['questions'] = 0;
}*/

//$_SESSION['questions'] = $_POST['questions'];
//$_SESSION['form1'] = $_POST['form1'];

//echo $_SESSION["results"]["Chemistry Adventure"]["Capital of Australia?"]["Brisbane"];
//$_POST['form1'] = 0;
if ($_SESSION['first'] == NULL) {
	if ($_POST['form1'] == NULL) {
		$_POST['form1'] = 0;
		$_SESSION['form1'] = 0;
	}
	if ($_POST['questions'] == NULL) {
		$_POST['questions'] = 0;
		$_SESSION['questions'] = 0;
	}
	$_SESSION['first'] = 1;
}
else {
	if ($_POST['form1'] != NULL) {
		$_SESSION['form1'] = $_POST['form1'];
		$_POST['questions'] = 0;
		$_SESSION['questions'] = 0;
	}	
	if ($_POST['questions'] != NULL) {
		$_SESSION['questions'] = $_POST['questions'];
	}
}

//echo "POST. form1: " . $_POST['form1'] . " questions: " . $_POST['questions']; 
//echo "SESSION. form1: " . $_SESSION['form1'] . " questions: " . $_SESSION['questions'];
//echo " umm: " . $question_name;
 
?>



<!DOCTYPE html>
<html lang="en">
<title>Analytics</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" href="<?php echo $_SESSION['url']; ?>/themes/default/images/favicon.ico">
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	
	<script>
	
	$(function() {
	
	});
	
	$(document).ready(function() {

		<?php $array = array_keys($_SESSION["results"]);
			$i = 0;
			foreach($array as $key) 
			{
				$key = str_replace("\"", "\'", $key);
				echo "$(\"#forms\").append(\"<option value = '" .  $i . "'" . selectedForm($i) . ">" . $array[$i] . "</option>\");";	
				$i = $i + 1;
			}
			function selectedForm($id) {
				if ($id == $_SESSION['form1']) {
					return " selected = 'selected'";
				}
			}

		?>	
		$("#vr").click(function(){});
	});
	$(document).ready(function() {
		<?php 
			$a = array_keys($_SESSION["results"]);
			$quiz_name = $a[$_SESSION['form1']];
			$array = array_keys($_SESSION["results"][$quiz_name]);
				$i = 0;
				foreach($array as $key) 
				{
					$key = str_replace("\"", "\'", $key);
					echo "$(\"#question\").append(\"<option value = '" . $i . "'" . selectedQ($i) . ">" . $key . "</option>\");";	
					$i = $i + 1;
				}
			function selectedQ($id) {
				if ($id == $_SESSION['questions']) {
					return " selected = 'selected'";
				}
			}
		?>
		$("#vq").click(function(){
			
		});
	});
	
		$(function() {
		
		<?php 
		    $a = array_keys($_SESSION["results"]);
			$quiz_name = $a[$_SESSION['form1']];
			$b = array_keys($_SESSION["results"][$quiz_name]);
			$question_name = $b[$_SESSION['questions']];
			$array = array_keys($_SESSION["results"][$quiz_name][$question_name]);
			$i = 0;
			$question_name = str_replace("\"", "\'", $question_name);
			echo "$(\"#q" . ($i + 1) . "\").append(\"<div>" . $question_name . "</div><h1></h1>\");";
			foreach($array as $key) {
				$key = str_replace("\"", "\'", $key);
				$percentage = $_SESSION["results"][$quiz_name][$question_name][$key];
				echo "$(\"#q" . ($i + 1) . "\").append(\"" . $key . "\");";
				$i++;
			}
		?>
	});
	
	$(function(){

		<?php 
		    $a = array_keys($_SESSION["results"]);
			$quiz_name = $a[$_SESSION['form1']];
			$b = array_keys($_SESSION["results"][$quiz_name]);
			$question_name = $b[$_SESSION['questions']];
			$array = array_keys($_SESSION["results"][$quiz_name][$question_name]);
			$i = "A";
			foreach($array as $key) {
				$percentage = $_SESSION["results"][$quiz_name][$question_name][$key];
				echo "$(\"#" . $i . "\").append(\"<div class='progress-bar progress-bar-success' role='progressbar' aria-valuenow='10' aria-valuemin='0' aria-valuemax='100' style='width:" . $percentage . "%'>" . $percentage . "%</div>\");";	
				$i++;
			}
		?>
	});

	</script>

</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="#" class="navbar-brand"></a>
			</div>

			<div>
				<ul class="nav navbar-nav">
					<li><a href="index.php">Create</a></li>
					<li><a href="preview.php">Preview</a></li>
					<li class="active"><a href="#">Analytics</a></li>
					<li><a href="<?php echo $_SESSION["url"] . "/index.php?logout"; ?>" class="no_border">Logout</a></li>
				</ul>
			</div>
		</div>
	</nav>
<div class = "col-md-2">
		<select name = "students"  id = "students" class = "form-control">
			<option>All Students</option>
		</select>
		<h1></h1>
		<div class="text-center" role="group" aria-label="...">
				<input id = "ss" type="submit" class="btn btn-primary btn-sm center-block" value = "Select Student">
		</div>
		<h1></h1>
	    <form method = "post">
		<select name = "form1" id = "forms" class="form-control">
		</select>
		<h1></h1>
		<div class="text-center" role="group" aria-label="...">
				<input id = "vq" type="submit" class="btn btn-primary btn-sm center-block" value = "View Questions">
		</div>
		</form>
		<h1></h1>
		<form method = "post">
		<select name = "questions"  id = "question" class = "form-control">
		</select>
		<h1></h1>
		<div class="text-center" role="group" aria-label="...">
				<input id = "vq" type="submit" class="btn btn-primary btn-sm center-block" value = "View Results">
		</div>
		<h1></h1>
		</form>
</div>
<span>
	<div class = "col-md-8">
	<h3><?php echo $_SESSION["score"]?></h3>	
	<h4 id = "q1" ></h4>
		<div id = "A" class="progress">
		</div>
	<h4 id = "q2" ></h4>
		<div id = "B" class="progress">
		</div>
	<h4 id = "q3" ></h4>
		<div id = "C" class="progress">
		</div>
	<h4 id = "q4"></h4>
		<div id = "D" class="progress">
		</div>
</span>
</div>

</body>

