<?php

require_once("../global/session_start.php");
ft_check_permission("client");

   if ((!isset($_SESSION["ft"]["account"]["submission_id"]) || empty($_SESSION["ft"]["account"]["submission_id"])) &&
       empty($_SESSION["ft"]["account"]["account_id"]))
{
        header("location: $g_root_url/modules/submission_accounts/logout.php");
      $message_flag = "notify_invalid_account_information_in_sessions";
      ft_logout_user($message_flag);
}
$_SESSION["url"] = $g_root_url;
_ft_cache_form_stats();

if ($_SESSION['page_number'] == NULL) $_SESSION['page_number'] = 1;
//echo "Next: " . $_POST['Next'] . " Back: " . $_POST['Back'];
//echo "Page number: " . $_SESSION['page_number'];

/*  <?php echo $_SESSION['page_number']; ?> */

require_once('create_form.php');

$title = $_POST["title"];
$question = $_POST["question"];
$link = $_POST["link"];
$correct = $_POST["correct"];
$alternate1 = $_POST["alternate1"];
$alternate2 = $_POST["alternate2"];
$alternate3 = $_POST["alternate3"];
$youtube1 = $_POST["youtube1"];
$youtube2 = $_POST["youtube2"];
$youtube3 = $_POST["youtube3"];

//store variables even if not all are set

if ($title != NULL) {
	$_SESSION["create"][1]["title"] = $title;
}
$array = array("question" => $question, "link" => $link, "correct" => $correct, "alternate1" => $alternate1, "alternate2" => $alternate2, 
		"alternate3" => $alternate3, "youtube1" => $youtube1, "youtube2" => $youtube2, "youtube3" => $youtube3);
foreach ($array as $key => $value) {
	if ($value != NULL) {
		$_SESSION["create"][$_SESSION['page_number']][$key] = $value;
	}
		//echo "ses: " . 
}
$_SESSION["create"][$_SESSION['page_number']]["page_type"] = "";

$page = test_variables_question($title, $question, $link);
$page_final = "";
$ids;

if ($page == "incorrect") {
	$page_final = "incorrect";
}
else {
	$ids = test_variables_options($correct, $alternate1, $alternate2, $alternate3, $youtube1, $youtube2, $youtube3);
	if ($ids == "incorrect") {
		$page_final = "incorrect";
	}
	else if ($page == "final_page" && $ids == "final_page") {
		$page_final = "final_page";
		$_SESSION["create"][$_SESSION["page_number"]]["page_type"] = "final_page";
	}
	else if ($page == "standard_page" && is_array($ids)) {
		$page_final = "standard_page";
		$_SESSION["create"][$_SESSION["page_number"]]["page_type"] = "standard_page";
	}
	else {
		$page_final = "incorrect";
	}
}


/*foreach (array_keys($_SESSION["create"]) as $page_n) {
	foreach ($array as $key => $value) {
		echo " Page number: " . $page_n . " Key: " . $key . " : " . $_SESSION["create"][$page_n][$key];
	}
}*/

if ($page_final != "incorrect") {
	//POST - 'Next' 
	$no_pages = count($_SESSION["create"]);
	if ($page_final == "standard_page") {
		$_SESSION["create"][$_SESSION['page_number']]["embed_link"] = "https://www.youtube.com/embed/" . $ids[0];
		$_SESSION["create"][$_SESSION['page_number']]["embed_y1"] = "https://www.youtube.com/embed/" . $ids[1];
		$_SESSION["create"][$_SESSION['page_number']]["embed_y2"] = "https://www.youtube.com/embed/" . $ids[2];
		$_SESSION["create"][$_SESSION['page_number']]["embed_y3"] = "https://www.youtube.com/embed/" . $ids[3];
	}
	if ($page_final == "final_page") {
		$_SESSION["create"][$_SESSION['page_number']]["embed_link"] = "https://www.youtube.com/embed/" . test_youtube_video($_SESSION["create"][$_SESSION['page_number']]["link"]);
	}
	if (($_POST['Create'] == 'Create')) 
	{ 
		if ($_SESSION["create"][$no_pages]["page_type"] != "final_page") {
			global $error_message;
			$error_message = "The final page (page " . $no_pages . ") must only contain the Quiz Title and Topic Youtube Video";
		}
		else if (count($_SESSION["create"]) < 2) {
			global $error_message; 
			$error_message = "Quiz must be more than one page long";
		}
		else {
			global $error_message;
			$array_final = create_quiz($_SESSION["create"][$no_pages]["embed_link"]);
			$error_message = $array_final["dir"];
			unset($_SESSION["create"]);
			$_SESSION['page_number'] = 1;
		}
	}
	if ($_POST['Next'] == 'Next') {
		if ($page_final != "final_page") {
	 		$_SESSION['page_number']++; 
		}
		else {
			global $error_message;
			$error_message = "No Question entered";
		}
	}
}

if ($error_message != "") $_POST["error"] = $error_message;


if (($_POST['Back'] == 'Back') && ($_SESSION['page_number'] > 1)) 
{ 
	$_SESSION['page_number']--;
}

//create_new_form ($conn, $title, $question, $link, $correct, $alternate1, $alternate2, $alternate3);

function test_variables_question($t, $q, $l)
{
	$final_page = "final_page";

 	if ($t == NULL) {
		global $error_message;
		$error_message = "No Quiz Title entered";
		return "incorrect";
	}
	$all_forms = ft_get_all_forms();
	foreach ($all_forms as $form)
	{
		if ($form['form_name'] == $t) {
			global $error_message;
			$error_message = "This Quiz Title already exists. Please choose another";
			return "incorrect";
		}
	}
 	if ($l == NULL) {
		global $error_message;
		$error_message = "No Topic Youtube Link entered";
		return "incorrect";
	}
	else {
		$id1 = test_youtube_video($l);
		if ($id1 == false) {
			global $error_message;
			$error_message = "Topic Youtube Link is invalid";
			return "incorrect";
		}
	}
 	if ($q == NULL) {
		if ($_SESSION['page_number'] == count($_SESSION["create"])) {
			return "final_page";
		}
		else {
			global $error_message;
			$error_message = "No Question entered";
			return "incorrect";
		}
	}
	else {
		return "standard_page";
	}
}

function test_variables_options($c, $a1, $a2, $a3, $y1, $y2, $y3)
{
	$page_type = "standard_page";
 	if ($c == NULL) {
		global $error_message; 
		$error_message = "No Correct Answer entered";
		$page_type = "final_page";
	}
 	if ($a1 == NULL) {
		if ($page_type != "final_page") {
			global $error_message;
			$error_message =  "No Alternate Answer 1 entered";
			return "incorrect";
		}
	}
 	if (($a2 == NULL) && ($y2 != NULL)) {
		if ($page_type != "final_page") {
			global $error_message; 
			$error_message =  "No Alternate Answer 2 entered";
			return "incorrect";
		}
	}
 	if (($a3 == NULL) && ($y3 != NULL)) {
		if ($page_type != "final_page") {
			global $error_message; 	
			$error_message = "No Alternate Answer 3 entered";
			return "incorrect";
		}
	}
 	if ($y1 == NULL && ($final_page != true)) {
		if ($page_type != "final_page") {
			global $error_message;
			$error_message = "No Alternate Answer 1 Youtube Link entered";
			return "incorrect";
		}
	}
 	if (($y2 == NULL) && ($a2 != NULL) && ($page_type == "standard_page")) {
		global $error_message;
		$error_message = "No Alternate Answer 2 Youtube Link entered";
		return "incorrect";
	}
	else if ($page_type == "final_page") {
		if (($y2 != NULL) || ($a2 != NULL)) {
			return "incorrect";
		}
	}
 	if (($y3 == NULL) && ($a3 != NULL) && ($page_type == "standard_page")) {
		global $error_message;
		$error_message = "No Alternate Answer 3 Youtube Link entered";
		return "incorrect";
	}
	else if ($page_type == "final_page") {
		if (($y3 != NULL) || ($a3 != NULL)) {
			return "incorrect";
		}
	}
	if ($page_type == "standard_page") {
		$id1 = test_youtube_video($l);
		$id2 = test_youtube_video($y1);
		$id3 = test_youtube_video($y2);
		$id4 = test_youtube_video($y3);
		if (($id2 == false)) {
			global $error_message;
			$error_message = "Alternate Answer 1 Youtube Link is invalid";
			return "incorrect";
		}
		if (($y2 != NULL) && ($id3 == false)) {
			global $error_message;
			$error_message = "Alternate Answer 2 Youtube Link is invalid";
			return "incorrect";
	
		}
		if (($y3 != NULL) && ($id4 == false)) {
			global $error_message;
			$error_message = "Alternate Answer 3 youtube link is invalid";
			return "incorrect";
		}
		return array($id1, $id2, $id3, $id4);
	}
	return $page_type; 
}

function test_youtube_video($url)
{
	preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $match);
	$id = $match[1];
	$testURL = "http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=" . $id . "&format=json";
	$headers = get_headers($testURL);
	
	if (substr($headers[0], 9, 3) !== "404") {
	    return $id;
	} else {
	    return false;
	}
}

function create_quiz($video) 
{
	$form_details = array() ; //create_new_form fills in
	$keys = array_keys($_SESSION["create"]);
	for ($i = 0; $i < count($keys) - 1; $i++) 
	{
		$c_t = $_SESSION["create"][$keys[$i]]["title"];
		$c_q = $_SESSION["create"][$keys[$i]]["question"];
		$c_l = $_SESSION["create"][$keys[$i]]["embed_link"];
		$c_c = $_SESSION["create"][$keys[$i]]["correct"];
		$c_a1 = $_SESSION["create"][$keys[$i]]["alternate1"];
		$c_a2= $_SESSION["create"][$keys[$i]]["alternate2"];
		$c_a3 = $_SESSION["create"][$keys[$i]]["alternate3"];
		$c_y1 = $_SESSION["create"][$keys[$i]]["embed_y1"];
		$c_y2 = $_SESSION["create"][$keys[$i]]["embed_y2"];
		$c_y3 = $_SESSION["create"][$keys[$i]]["embed_y3"];
		if ($keys[$i] == 1)  {
			$form_details = create_new_form($c_t, $c_q, $c_l, $c_c, $c_a1, $c_a2, $c_a3, $c_y1, $c_y2, $c_y3);
		}
		else {
			insert_new_question($form_details["form_id"], $form_details["view_id"], $c_q, $c_l, $c_c, $c_a1, $c_a2, $c_a3, $c_y1, $c_y2, $c_y3);
		}
	}
	update_final_page_video($form_details["form_id"], $video);
	return $form_details;
}
?>


<!DOCTYPE html>
<html lang="en">
<title>Create</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" href="<?php echo $_SESSION['url']; ?>/themes/default/images/favicon.ico">

<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	
</head>
<body>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a href="#" class="navbar-brand"></a>
			</div>

			<div class = "row">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Create</a></li>
					<li><a href="preview.php">Preview</a></li>
					<li><a href="analytics.php">Analytics</a></li>
					<li><a href="<?php echo $_SESSION["url"] . "/index.php?logout"; ?>" class="no_border">Logout</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div class = "row">
		<div class="col-lg-1"></div>
		<span class ="badge">Page <?php echo $_SESSION['page_number']; ?></span>
		<h1></h1>
	</div>
	<div class="row">
		<div class="col-md-1"></div>
			<div class = "col-md-5">
				<h5><?php if (isset($_POST["error"])) echo $_POST["error"];?></h5>
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1">Quiz Title:</span>
					<form action="index.php" method="post">
					<input required type="text" class="form-control" aria-describedby="basic-addon1" name="title"<?php 
	
							if ($_SESSION["create"][1]["title"]!=NULL) {
								echo " value=\"" . $_SESSION["create"][1]["title"] . "\"";
							}
																?>>
					<form/>
				</div>
			</div>
		<div class="col-md-6"></div>
	</div>
	<h1></h1>
	<p></p>
	<div class="row">
		<div class="col-md-1"></div>
			<div class = "col-md-5">
				<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Topic Youtube Link:</span>
				<form action="index.php" method="post">
				<input type="text" class="form-control" aria-describedby="basic-addon1" name="link"<?php 
	
							if ($_SESSION["create"][$_SESSION['page_number']]["link"]!=NULL) {
								echo " value=\"" . $_SESSION["create"][$_SESSION['page_number']]["link"] . "\"";
							}
																?>>
				<form/>
			</div>
			</div>
		<div class="col-md-6"></div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-md-1"></div>
		<div class = "col-md-5">
			<div class="input-group">
					<span class="input-group-addon" id="basic-addon1">Question:</span>
					<form action="index.php" method="post">
					<input type="text" class="form-control" aria-describedby="basic-addon1" name="question"<?php 
	
							if ($_SESSION["create"][$_SESSION['page_number']]["question"]!=NULL) {
								echo " value=\"" . $_SESSION["create"][$_SESSION['page_number']]["question"] . "\"";
							}
																?>>
					<form/>
				</div>
			
		</div>
		<div class="col-md-6"></div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-md-1"></div>
		<div class = "col-md-5">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Correct Answer:</span>
				<form action="index.php" method="post">
				<input type="text" class="form-control" aria-describedby="basic-addon1" name="correct"<?php 
	
							if ($_SESSION["create"][$_SESSION['page_number']]["correct"]!=NULL) {
								echo " value=\"" . $_SESSION["create"][$_SESSION['page_number']]["correct"] . "\"";
							}
																?>>
				<form/>
			</div>
		</div>
		<div class="col-md-6"></div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-md-1"></div>
		<div class = "col-md-5">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Alternate Answer:</span>
				<form action="index.php" method="post">
				<input type="text" class="form-control" aria-describedby="basic-addon1" name="alternate1"<?php 
	
							if ($_SESSION["create"][$_SESSION['page_number']]["alternate1"]!=NULL) {
								echo " value=\"" . $_SESSION["create"][$_SESSION['page_number']]["alternate1"] . "\"";
							}
																?>>
				<form/>
			</div>
			<div class="col-md-6"></div>
		</div>
		<div class = "col-md-5">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Youtube Link:</span>
				<form action="index.php" method="post">
				<input type="text" class="form-control" aria-describedby="basic-addon1" name="youtube1"<?php 
	
							if ($_SESSION["create"][$_SESSION['page_number']]["youtube1"]!=NULL) {
								echo " value=\"" . $_SESSION["create"][$_SESSION['page_number']]["youtube1"] . "\"";
							}
																?>>
				<form/>
			</div>
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-md-1"></div>
		<div class = "col-md-5">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Alternate Answer:</span>
				<form action="index.php" method="post">
				<input type="text" class="form-control" aria-describedby="basic-addon1" name="alternate2"<?php 
	
							if ($_SESSION["create"][$_SESSION['page_number']]["alternate2"]!=NULL) {
								echo " value=\"" . $_SESSION["create"][$_SESSION['page_number']]["alternate2"] . "\"";
							}
																?>>
				<form/>
			</div>
		</div>
		<div class = "col-md-5">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Youtube Link:</span>
				<form action="index.php" method="post">
				<input type="text" class="form-control" aria-describedby="basic-addon1" name="youtube2"<?php 
	
							if ($_SESSION["create"][$_SESSION['page_number']]["youtube2"]!=NULL) {
								echo " value=\"" . $_SESSION["create"][$_SESSION['page_number']]["youtube2"] . "\"";
							}
																?>>
				<form/>
			</div>
		</div>
	</div>
	<p></p>
	<div class="row">
		<div class="col-md-1"></div>
			<div class="col-md-5">
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1">Alternate Answer:</span>
					<form action="index.php" method="post">
					<input type="text" class="form-control" aria-describedby="basic-addon1" name="alternate3"<?php 
	
							if ($_SESSION["create"][$_SESSION['page_number']]["alternate3"]!=NULL) {
								echo " value=\"" . $_SESSION["create"][$_SESSION['page_number']]["alternate3"] . "\"";
							}
																?>>
					<form/>
				</div>
			</div>
		<div class = "col-md-5">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Youtube Link:</span>
				<form action="index.php" method="post">
				<input type="text" class="form-control" aria-describedby="basic-addon1" name="youtube3"<?php 
	
							if ($_SESSION["create"][$_SESSION['page_number']]["youtube3"]!=NULL) {
								echo " value=\"" . $_SESSION["create"][$_SESSION['page_number']]["youtube3"] . "\"";
							}
																?>>
				<form/>
			</div>
		</div>

	</div>
	<h1></h1>
	<div class = "row">
	<div class = "text-center">
			<form action = "index.php" method="post">
			<div class="text-center" role="group" aria-label="...">
					<input onclick="myFunction()"type="submit" name="Next" class="btn btn-primary btn-lg center-block" value = "Next"></input>
					<input type="submit" name="Back" class="btn btn-primary btn-lg center-block" value = "Back"></input>
					<input type="submit" name="Create" class="btn btn-primary btn-lg center-block" value = "Create"></input>
			</div>
			</form>
			</div>
			<p></p>
		</div>
	</div>
	</div>


				
</body>
</html>
<p></p>

<?php
//require_once("insert.php");
//	echo ft_option_lists_old2('', "TEST", "no", NULL);
	

//$conn = new mysqli($g_db_hostname, $g_db_username, $g_db_password, $g_db_name);


?>
