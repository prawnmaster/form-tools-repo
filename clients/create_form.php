<?php
require_once("insert.php");
require_once("../global/config.php");

ini_set('display_errors',1);

global $g_db_servername;
global $g_db_username;
global $g_db_password;
global $g_db_name;

// Create connection
$conn = new mysqli($g_db_servername, $g_db_username, $g_db_password, $g_db_name);


//require_once("create_form.php");
//create_new_form ($conn, "test 2", "answer to life", "link", "C_A", "A_A1", "A_A2","A_A3");
make_published_file("form_test 2.php", 9);
//insert_new_question($conn, 12, 15, "whats up?", 'ze link', "correct ans2", "alt12", "alt22",
 //"alt32");
//echo $g_root_dir;
//order of paramaters in insert.php does not neccesarily have same order as columns in DB 
//mysqli_insert_id() needs the connection as the parameter, naming it con for the time being
//Sam - the form tools sessions deals with this. Can just leave it as mysqli_insert_id(), no param needed
//require_once("create_form.php");
//create_new_form ($conn, "title", "Q1", "link", "C_A", "A_A1", "A_A2","A_A3");

/**	
* create a new form using client input
* TODO insert youtube links
* TODO insert wrong/right field into options   
* TODO add table for submissions
*/
function create_new_form ($form_title, $form_question, $youtube_link, $correct_answer, $alternate_answer1, $alternate_answer2,
 $alternate_answer3, $alternate_youtube_link1, $alternate_youtube_link2, $alternate_youtube_link3)
{
	global $g_db_servername;
	global $g_db_username;
	global $g_db_password;
	global $g_db_name;

	$conn = new mysqli($g_db_servername, $g_db_username, $g_db_password, $g_db_name);

	$datetime = time();
	
	//form id is auto increment
	ft_forms($conn, '', 'form_builder', 'public', Null, $datetime, 'yes', 'yes', 'yes', 'no',$form_title, '', '', 'yes', 'yes', "Edit Submission", 
	'$LANG.word_add_rightarrow');
	$form_id = mysqli_insert_id($conn);
	//$form_id = MySqli_insert_id($con); //get form id from last auto generated PK
	//NOTE: This has to go before the function
	make_table($form_id, "col_2");
	
	ft_list_groups($conn, '', "form_{$form_id}_view_group", 'Views', '', 1);  
	$view_group_id = MySqli_insert_id($conn);
	//view id is auto increment
	ft_views($conn, '', $form_id, 'public', "Form Builder View", 1, 'yes', $view_group_id, 10, "submission_date", 'desc', 'yes', 'yes', 'yes', 
	'no', 'no');
	$view_id = mysqli_insert_id($conn);
	
	$view_field_group = get_view_field_group_type()+1;
	ft_list_groups($conn, '', "view_fields_{$view_id}", ' ', 1, 1);   
	$view_field_login_group_id = MySqli_insert_id($conn);
	ft_list_groups($conn, '', "view_fields_{$view_id}", ' ', 2, 2);   	$view_field_group_id = MySqli_insert_id($conn);
	
	ft_view_tabs($conn, $view_id, 1, 'Login');
	ft_view_tabs($conn,$view_id, 2, 'Page 1');
	
	//list id is auto increment
	ft_option_lists($conn, '', $form_question, 'no', NULL); 
	 $list_id = mysqli_insert_id($conn);
	
	//ft_list_groups($conn, '', get_option_field_group_type(), '', '', 1);
	ft_list_groups($conn, '', "option_list_{$list_id}", '', '', 1);
	$group_id = mysqli_insert_id($conn);
	//should check whether additional alternate answers are filled?
	ft_field_options($conn, $list_id, $group_id, 1, "correct_answer", $correct_answer, 'yes', 'yes', ''); 
	ft_field_options($conn, $list_id, $group_id, 2, "alternate answer1", $alternate_answer1, 'yes', 'no', $alternate_youtube_link1); 
	if (!empty($alternate_answer2)){
	ft_field_options($conn, $list_id, $group_id, 3, "alternate answer2", $alternate_answer2, 'yes', 'no', $alternate_youtube_link2); }
	if (!empty($alternate_answer3)){
    ft_field_options($conn, $list_id, $group_id, 4, "alternate answer3", $alternate_answer3, 'yes', 'no', $alternate_youtube_link3); }	
	//field id is auto increment
	//below fills in form_fields, view_colums, view_fields, field_settings
	//need field id so enter stuff for one field at a tim 
	ft_form_fields($conn, '', $form_id, "core__submission_id", '', 'medium', 1, 'yes', 'number',
	'ID', "submission_id", 1, 'yes', 'no', '');
	$field_id = mysqli_insert_id($conn);
	ft_view_fields($conn, $view_id, $field_id, $view_field_login_id, 'no', 'yes', 1, 'yes'); 
	ft_view_columns($conn, $view_id, $field_id, 1, 'yes', 'no', 160, 'truncate');
	
	ft_form_fields($conn, '', $form_id, "textbox", 1, 'medium', 1, 'no', 'string', 'Please enter your student number: ', 
	'col_1', 2,'yes', 'no', '' ); 
	$field_id = mysqli_insert_id($conn);
	ft_field_settings($conn, $field_id, 4, '');
	ft_view_fields($conn, $view_id, $field_id, $view_field_login_group_id,'yes', 'yes', 2, 'yes');
	ft_view_columns($conn, $view_id, $field_id, 2, 'yes', 'yes', '', 'truncate');
	
	ft_form_fields($conn, '', $form_id, "field", 1, 'medium', 6, 'no', 'string',
	$form_question, "col_2", 3, 'yes', 'no', $youtube_link);
	$field_id = mysqli_insert_id($conn);
	ft_field_settings($conn, $field_id, 16, $list_id);
	ft_view_fields($conn, $view_id, $field_id, $view_field_group_id, 'yes', 'yes', 3, 'yes'); 
	ft_view_columns($conn, $view_id, $field_id, 3, 'yes', 'yes', '', 'truncate');
	
	ft_form_fields($conn, '', $form_id, "core__submission_date", '', 'medium', 8, 'yes', 'date',
	'Date', "submission_date", 4, 'yes', 'no', ''); 
	$field_id = mysqli_insert_id($conn);
	ft_field_settings($conn, $field_id, 22, 'datetime:yy-mm-dd|h:mm TT|ampm`true');
	ft_field_settings($conn, $field_id, 23, 'yes');
	ft_view_fields($conn, $view_id, $field_id, $view_field_login_group_id, 'no', 'yes', 4, 'yes'); 
	ft_view_columns($conn,$view_id, $field_id, 4, 'yes', 'no', 160, 'truncate');
	 
	ft_form_fields($conn, '', $form_id, "core__last_modified", '', 'medium', 8, 'yes', 'date',
	'Last modified', "last_modified_date", 5, 'yes', 'no', '');
	$field_id = mysqli_insert_id($conn);
	ft_field_settings($conn, $field_id, 22, 'datetime:yy-mm-dd|h:mm TT|ampm`true');
	ft_field_settings($conn, $field_id, 23, 'yes');
	ft_view_fields($conn, $view_id, $field_id, $view_field_login_group_id, 'no', 'yes', 5, 'yes'); 
	
	ft_form_fields($conn, '', $form_id, "core__ip_address", '', 'medium', 1, 'yes', 'IP Address',
	'IP Address', "ip_address", 6, 'yes', 'no', '');
	$field_id = mysqli_insert_id($conn);
	ft_view_fields($conn, $view_id, $field_id, $view_field_login_group_id, 'no', 'yes', '', 'yes'); 
	//published form builder auto increment
	//TODO:
	//rather than fixed urls and dirs, use config.php vars below:
	//require_once("../global/library.php");
	//require_once('C:\xampp\htdocs\formtools\global\library.php');
	global $g_root_url;
	global $g_root_dir;
	$dir = $g_root_dir . "/modules/form_builder/published";
	$url = $g_root_url . "/modules/form_builder/published";
	
	//$dir = "C:xampp/htdocs/formtools/modules/form_builder/published";
	//$url = "http://localhost/formtools/modules/form_builder/published";
    
	ft_module_form_builder_forms($conn, '', 'yes', 'yes', $form_id, $view_id, 1, $datetime, $form_title, $dir, $url, 'no',
	'no', '<h2 class="ts_heading">Thanks!</h2><p>Thank you for completing this Chemistry Adventure</p>',
	'<h2 class="ts_heading">Sorry!</h2><p>The form is currently offline.</p>', 
	'Review', 'Thankyou', '0000-00-00 00:00:00', 1);
	$published_form_id = MySqli_insert_id($conn); 
	
	ft_module_form_builder_form_placeholders($conn, $published_form_id, 1, "Blue-Grey");
	
    ft_module_form_builder_form_templates($conn, $published_form_id, "continue_block", 9);
	ft_module_form_builder_form_templates($conn, $published_form_id, "error_message", 14);
	ft_module_form_builder_form_templates($conn, $published_form_id, "footer", 4);
	ft_module_form_builder_form_templates($conn, $published_form_id, "form_offline_page", 8);
	ft_module_form_builder_form_templates($conn, $published_form_id, "form_page", 5);
	ft_module_form_builder_form_templates($conn, $published_form_id, "header", 2);
	ft_module_form_builder_form_templates($conn, $published_form_id, "navigation", 11);
	ft_module_form_builder_form_templates($conn, $published_form_id, "page_layout", 1);
	ft_module_form_builder_form_templates($conn, $published_form_id, "review_page", 6);
	ft_module_form_builder_form_templates($conn, $published_form_id, "thankyou_page", 7);
	ft_module_form_builder_form_templates($conn, $published_form_id, "back_block", 15);
	ft_module_form_builder_form_templates($conn, $published_form_id, "youtube_embed", 16);

	make_published_file("form_{$form_title}.php", $published_form_id);
	$file_location = $g_root_url . "/modules/form_builder/published/form_" . $form_title . ".php";
	$return_array = array("dir" => $file_location, "form_id" => $form_id, "view_id" => $view_id);
	ft_save_file_location($conn, $form_id, $file_location);
	return $return_array;
}

function update_final_page_video($form_id, $video)
{
	global $g_db_servername;
	global $g_db_username;
	global $g_db_password;
	global $g_db_name;

	$conn = new mysqli($g_db_servername, $g_db_username, $g_db_password, $g_db_name);

	final_page_video($conn, $form_id, $video);
}

  /**
  * deletes form and all of its tables in DB with specified form_id
  * Needs the unique form id
  * All references to the deleted form should be deleted from the database as to not clog it up 
  * no foreign keys, all deletion done manually 
  */

function delete_form($form_id)
 {
	global $g_db_servername;
	global $g_db_username;
	global $g_db_password;
	global $g_db_name;

	$error_message;

	$conn = new mysqli($g_db_servername, $g_db_username, $g_db_password, $g_db_name);

	 global $g_table_prefix;
	   // remove the submissions table
    $query = "DROP TABLE IF EXISTS {$g_table_prefix}form_$form_id";
	if ($conn->query($query) === TRUE) {
	    //echo "delete successfull";
		}
	else {$error_message = "Error: " . $query . "<br>" . $conn->error;}
	
   //Deletes in specific order 
   //remove field settings 
   $query = "SELECT DISTINCT field_id FROM {$g_table_prefix}form_fields WHERE form_id = $form_id";
   $result = $conn->query($query);
   while($row = $result->fetch_assoc()) {
	   //get the field id thats associated with the setting
		$field = $row["field_id"];
		$query2 = "SELECT setting_value FROM {$g_table_prefix}field_settings WHERE field_id = $field AND
		setting_id = 16";
		$result2 = $conn->query($query2);
		while($row2 = $result2->fetch_assoc()) {
			//delete field options + option lists associated with setting value
			$list = $row2["setting_value"];
			$query3 = "DELETE FROM {$g_table_prefix}field_options WHERE list_id = $list";
			if ($conn->query($query3) === TRUE) { //echo "delete successfull";
							}
			else {$error_message = "Error: " . $query3 . "<br>" . $conn->error;}
			$query3 = "DELETE FROM {$g_table_prefix}option_lists WHERE list_id = $list";
			if ($conn->query($query3) === TRUE)  { //echo "delete successfull";
							}
			else {$error_message = "Error: " . $query3 . "<br>" . $conn->error;}
			//delete option list list group
			$query3 ="DELETE FROM {$g_table_prefix}list_groups WHERE group_type = 'option_list_{$list}'";
			if ($conn->query($query3) === TRUE) {//echo "delete successfull";
							}
	        else {$error_message = "Error: " . $query3 . "<br>" . $conn->error;}
		}
		//finally actually delete the field setting
		$queryseption = "DELETE FROM {$g_table_prefix}field_settings WHERE field_id = $field"; 
		if ($conn->query($queryseption) === TRUE) {//echo "delete successfull";
							}
	    else {$error_message = "Error: " . $queryseption . "<br>" . $conn->error;}
   }
 
  // remove any reference to the form in form_fields
   $query = "DELETE FROM {$g_table_prefix}form_fields WHERE form_id = $form_id";
   if ($conn->query($query) === TRUE) {
	   // echo "delete successfull";
				}
	else {$error_message = "Error: " . $query . "<br>" . $conn->error;} 
   
  // remove any reference to the form in forms table
    $query = "DELETE FROM {$g_table_prefix}forms WHERE form_id = $form_id";
	if ($conn->query($query) === TRUE) {
	    //echo "delete successfull";
				}
	else {$error_message = "Error: " . $query . "<br>" . $conn->error;}
    
	// remove list group reference
	$query ="DELETE FROM {$g_table_prefix}list_groups WHERE group_type = 'form_{$form_id}_view_group'";
	if ($conn->query($query) === TRUE) {
	    //echo "delete successfull";
				}
	else {$error_message = "Error: " . $query . "<br>" . $conn->error;}

    // delete all form Views, view_fields, view_columns, view groups
	$query = "SELECT DISTINCT view_id FROM {$g_table_prefix}views WHERE form_id = $form_id";
	$result = $conn->query($query);
	   while($row = $result->fetch_assoc()) {
	      $view = $row["view_id"];
		  //delete tabs
		  $query2 = "DELETE FROM {$g_table_prefix}view_tabs WHERE view_id = $view";
		  if ($conn->query($query2) === TRUE) { //echo "delete successfull";
						}
		  else {$error_message = "Error: " . $query2 . "<br>" . $conn->error;}
		  //delete view_columns 
		  $query2 = "DELETE FROM {$g_table_prefix}view_columns WHERE view_id = $view";
		  if ($conn->query($query2) === TRUE) { //echo "delete successfull";
						}
		  else {$error_message = "Error: " . $query2 . "<br>" . $conn->error;}
		  $query3 = "SELECT DISTINCT `group_id` FROM `ft_view_fields` WHERE `view_id` = $view";
		  $result2 = $conn->query($query3);
		  //delete view_field group
		  while($row2 = $result2->fetch_assoc()) {
			  $group = $row2["group_id"];
			  $query4 = "DELETE FROM {$g_table_prefix}list_groups WHERE group_id = $group";
			  if ($conn->query($query4) === TRUE) { //echo "delete successfull";
					}
		      else {$error_message = "Error: " . $query4 . "<br>" . $conn->error;}
		  }
		  $query2 = "DELETE FROM {$g_table_prefix}view_fields WHERE view_id = $view";
		  if ($conn->query($query2) === TRUE) { //echo "delete successfull";
					}
		  else {$error_message = "Error: " . $query2 . "<br>" . $conn->error;}
	    }

    $query = "DELETE FROM {$g_table_prefix}views WHERE form_id = $form_id";
	if ($conn->query($query) === TRUE) {
	   // echo "delete successfull";
				}
	else {$error_message = "Error: " . $query . "<br>" . $conn->error;}

	//get published id and delete data from form_builder_module tables
	$query = "SELECT DISTINCT published_form_id FROM {$g_table_prefix}module_form_builder_forms WHERE form_id = $form_id";
	$result = $conn->query($query);
	   while($row = $result->fetch_assoc()) {
		   $published = $row["published_form_id"];
		   //delete placeholder
		   $query2 = "DELETE FROM {$g_table_prefix}module_form_builder_form_placeholders WHERE published_form_id = $published";
	       if ($conn->query($query2) === TRUE) { //echo "delete successfull";
						}
	       else {$error_message = "Error: " . $query2 . "<br>" . $conn->error;}
		   //delete template
		   $query2 = "DELETE FROM {$g_table_prefix}module_form_builder_form_templates WHERE published_form_id = $published";
	       if ($conn->query($query2) === TRUE) { //echo "delete successfull";
						}
	       else {$error_message = "Error: " . $query2 . "<br>" . $conn->error;}
		   //delete published
		   $query2 = "DELETE FROM {$g_table_prefix}module_form_builder_forms WHERE published_form_id = $published";
	       if ($conn->query($query2) === TRUE) { //echo "delete successfull";
						}
	       else {$error_message = "Error: " . $query2 . "<br>" . $conn->error;}
		   
	   }
	return $error_message;
}


/**
* inserts a new question into an existing form
* adds a new field, option, tab
* TODO group list names, field names, column names, list order for editable fields need to be correct
* TODO update ft_tabs needs to be correct 
*/
function insert_new_question($form_id, $view_id, $form_question, $youtube_link, $correct_answer, $alternate_answer1, $alternate_answer2,
 $alternate_answer3, $alternate_youtube_link1, $alternate_youtube_link2, $alternate_youtube_link3)
 {
	ini_set('display_errors',1);
	 //tab number + label should reflect the actual page where the question is added 
	global $g_db_servername;
	global $g_db_username;
	global $g_db_password;
	global $g_db_name;


	$conn = new mysqli($g_db_servername, $g_db_username, $g_db_password, $g_db_name);

	$datetime = time();

	$nextTab = get_number_of_tabs($conn, $view_id)+1;
	$page_no = $nextTab - 1;
	ft_view_tabs($conn, $view_id, $nextTab, "Page {$page_no}");
	
	//view field for next page		
	ft_list_groups($conn,'', "view_fields_{$view_id}"," ", $nextTab, $nextTab);
	$view_field_group = mysqli_insert_id($conn);
	
	ft_option_lists($conn, '', $form_question, 'no', Null); 
	$list_id = mysqli_insert_id($conn);
	
	//new option list group
	ft_list_groups($conn,'', "option_list_{$list_id}", '', '', 1);
	$group_id = mysqli_insert_id($conn);
	
	ft_field_options($conn,$list_id, $group_id, 1, "correct answer", $correct_answer, 'yes', 'yes', ''); 
	ft_field_options($conn,$list_id, $group_id, 2, "alternate answer1", $alternate_answer1, 'yes', 'no', $alternate_youtube_link1); 
	if (!empty($alternate_answer2)){
	ft_field_options($conn,$list_id, $group_id, 3, "alternate answer2", $alternate_answer2, 'yes', 'no', $alternate_youtube_link2); }
	if (!empty($alternate_answer3)){
    ft_field_options($conn,$list_id, $group_id, 4, "alternate answer3", $alternate_answer3, 'yes', 'no', $alternate_youtube_link3); }
	
	$numOfRadioFields = number_of_radio_fields($conn, $form_id) + 1;
	$numOfFields = get_number_of_fields($conn, $form_id)+ 1;
	ft_form_fields($conn,'', $form_id, "field", 1, 'medium', 6, 'no', 'string', $form_question, "col_{$numOfRadioFields}", 
	$numOfFields, 'yes', 'no', $youtube_link);
	$field_id = mysqli_insert_id($conn); 
	ft_field_settings($conn,$field_id, 16, $list_id);
	ft_view_fields($conn,$view_id, $field_id, $view_field_group, 'yes', 'yes', $numOfFields, 'yes'); 
	ft_view_columns($conn,$view_id, $field_id, $numOfFields, 'yes', 'yes', '', 'truncate');
	
	
	update_table($conn, "ft_form_{$form_id}", "col_{$numOfRadioFields}");
	
 }

 /**
 * used to update the submission table when inserting a new question
 */
 function update_table($conn, $table_name, $col_name){
	global $conn;
    global $g_table_prefix;
    $table_query = "ALTER TABLE {$table_name} ADD {$col_name} varchar(255)";
    $result = mysqli_query($conn, $table_query);
    return $result;
	 
 }

 /**
 * function to update view tabs
 * could be added to insert.php
 */
function update_ft_tabs($view_id, $tab_number, $tab_label)
{
	global $g_table_prefix;
    $update_query = "UPDATE {$g_table_prefix}ft_view_tabs SET `tab_label`= {$tab_label} WHERE tab_number = {$tab_number}";
	$result = mysqli_query($update_query);
	return $result;
}
 
 /**
 * makes a table used for submissions
 */
 function make_table($form_count, $question)
 {
    global $conn;
    global $g_table_prefix;
    $table_query = "CREATE TABLE ft_form_{$form_count} (submission_id mediumint(8) AUTO_INCREMENT, col_1 varchar(255), {$question} varchar(255),submission_date datetime, 
    last_modified_date datetime, ip_address varchar(15), is_finalized enum('yes', 'no'), link_id mediumint(8), attempt_id mediumint(8),  PRIMARY KEY (submission_id))";
    $result = mysqli_query($conn, $table_query);
    return $result;
 }
 
 /**
 * used in the group type column in ft_list_groups
 * uses a number one bigger then largest view ID
 */
 function get_view_field_group_type()
{
   $num = -1;
   global $conn;
   $sql = "SELECT view_id FROM ft_views";
   $result = $conn->query($sql);
   if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        //echo "id: " . $row["view_id"]. "<br>";
		if($row["view_id"] > $num){
			$num = $row["view_id"];
		} 
    }
    }
   $num = $num;
}

/**
 * used in the group type column in ft_list_groups
 * uses a number one bigger then largest list ID
 */
function get_option_field_group_type()
{
  $num = -1;  
  global $conn;
  $sql = "SELECT list_id FROM ft_option_lists";
  $result = $conn->query($sql);
   if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        //echo "id: " . $row["view_id"]. "<br>";
		if($row["list_id"] > $num){
			$num = $row["list_id"];
		} 
    }
    } 
  $num = $num + 1;
  return "option_list_{$num}";
}

/**
 * gets number of radio button fields in a form 
 * used for column name and field title
 */
function number_of_radio_fields($conn, $form_id)
{ 
   $num = 0;
   global $conn;
   $sql = "SELECT form_id FROM ft_form_fields where form_id = $form_id and is_system_field = 'no'";
   $result = $conn->query($sql);
   if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        //echo "id: " . $row["view_id"]. "<br>";
			$num ++;
		
    }
    }
   return $num;
}

/**
 * gets number of fields in form
 * used to get next list order
 */
function get_number_of_fields($conn, $form_id)
{ 
   $num = 0;
   global $conn;
   $sql = "SELECT COUNT(*) FROM `ft_form_fields` WHERE form_id = {$form_id}";
   $result = $conn->query($sql);
   if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        //echo "id: " . $row["view_id"]. "<br>";
			$num = $row["COUNT(*)"];		
    }
    }
   return $num;
}

/**
 * gets number of tabs in form
 * used to make next tab
 */
function get_number_of_tabs($conn, $view_id)
{
   $num = 0;
   global $conn;
   $sql = "SELECT COUNT(*) FROM `ft_view_tabs` WHERE view_id = $view_id";
   $result = $conn->query($sql);
   if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        //echo "id: " . $row["view_id"]. "<br>";
			$num = $row["COUNT(*)"];		
    }
    }
   return $num;
}


function make_published_file($filename, $published_form_id){
	global $g_root_dir;
	$dir = $g_root_dir . "/modules/form_builder/published";
	$library_dir = $g_root_dir . '/global/library.php';
	
	$myfile = fopen("{$dir}/{$filename}", "w");
	$txt = "<?php\n";
	fwrite($myfile, $txt);
	$txt = "require_once('$library_dir');\n";
	fwrite($myfile, $txt);
	$txt = "$";
	fwrite($myfile, $txt);
	$txt = "published_form_id = $published_form_id;\n";
	fwrite($myfile, $txt);
	$txt = "$";
	fwrite($myfile, $txt);
	$txt = "filename  = '$filename';\n";
	fwrite($myfile, $txt);
	$txt = 'require_once("$';
	fwrite($myfile, $txt);
	$txt = 'g_root_dir/modules/form_builder/form.php");';
	fwrite($myfile, $txt);
    fclose($myfile);
	return $dir . "/" . $filename;
}


?>


















